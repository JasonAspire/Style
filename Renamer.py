import os,sys
   
import json


import docx
import copy

import constants

def getText(filename):
    doc = docx.Document(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return '\n'.join(fullText)

userCSSTemplate = [
    """ /* ==UserStyle==
@name nameFill
@namespace gitlab.com/JasonAspire/Style
@homepageURL https://gitlab.com/JasonAspire/Style
@version 1.0.8
@updateURL urlFill
@description Todo 
@author Lauren Birnhak
@license GNU-V3.0
==/UserStyle== */
"""
]


jsonTemplate = [{
		"enabled": "false",
		"updateUrl": "",
		"md5Url": "null",
		"url": "null",
		"originalMd5": "null",
		
		"name": "",
		"sections": [
			{
                "code": "null",
                "domain":"@-moz-document domain(example.com)",
			}
		]
		
	}]


folder = 'Stylus-Themes'
for filename in os.listdir(folder):
    if filename.endswith('.docx'):
        infilename = os.path.join(folder,filename)
        if not os.path.isfile(infilename): continue
        oldbase = os.path.splitext(filename)
        
        urlEnd = str(infilename).split('/')[1].split('.')[0].replace(' ','-') 
        urlComplete = constants.HOME_URL_BASE 
        urlUpdate = constants.HOME_URL_UPDATE + urlEnd +'.user.css'
        baseName = filename.split(".")[0]

        #content of Docxfile.
        localText = getText(infilename)

        try:
            localText.index('/* ==UserStyle==')
        except ValueError:
            TempHeader = copy.deepcopy(userCSSTemplate[0]).replace('urlFill', urlUpdate).replace('nameFill', baseName)
            localText =  TempHeader+localText
        #Check if first of the lines are /* ===UserStyle==



        jsonTemplate[0]["sections"][0]["code"] = localText
        jsonTemplate[0]["name"] = filename.split(".")[0]
        jsonTemplate[0]['url'] = urlComplete 
        jsonTemplate[0]['updateUrl'] = urlUpdate 

        #jsonFile = json.dumps(jsonTemplate, indent=2)
        

        ##Send updated Json file to new folders
        newName = baseName +'.json'
        cssName = baseName+'.user.css'

        completeName = os.path.join('Stylus-JsonV2', newName)   
        with open(completeName, 'w') as outfile:
            json.dump(jsonTemplate, outfile, indent=2 )



        #mass update CSS files to user.css
        cssName = os.path.join('Stylus-CSS', cssName)
        with open(cssName, 'w') as outfile:
            outfile.write(localText)




    
    
    