import os,sys
   
import json


import docx
import copy



def getText(filename):
    doc = docx.Document(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return '\n'.join(fullText)





folder = 'Stylus Themes'
for filename in os.listdir(folder):
    if filename.endswith('.docx'):
        infilename = os.path.join(folder,filename)
        if not os.path.isfile(infilename): continue
        oldbase = os.path.splitext(filename)
        

        localText = getText(infilename)
       
        #jsonFile = json.dumps(jsonTemplate, indent=2)
        
        newName = filename.split(".")[0]+'.css'
        completeName = os.path.join('Stylus CSS', newName)   
        with open(completeName, 'w') as outfile:
           
            outfile.write(localText)




    
    
    